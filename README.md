# BITSMITHS!

This is the repository for the Bitsmiths team in Dr. Morse's CS Senior Project class.
Class work and projects will be stored here. 

* * *

## Our Project
The Twitch Channel Guide (TCG) is a web app that helps Twitch users better navigate Twitch. Twitch is a broadcasting service specifically targeted at video game players. Users can stream their game play from a personal computer, video game console or handheld device. Users will be able to log in to the TCG with their Twitch account. On their TCG account users can set their own streaming schedule. A user will be able to see when a streamer is going to stream as long as the user has followed the streamer. Users will also  be able to change the time scope of their guide. Unlike Twitch's current navigation, the TCG will navigate Twitch using a schedule generated for each user. The Twitch Channel Guides purpose is to simplify and enhance each user's streaming experience.

### Project Status
Base construction beginning soon!

Deployed site: [TwitchChannelGuide.Azurewebsites.net](TwitchChannelGuide.Azurewebsites.net)

### Building
Please build our project using Visual Studio 2015

### Guidelines
* Use the table name in the Primary key name, i.e ProductID
* All public methods include XML comments
* Curly Braces on their own lines
* All work is to be done on a new branch per pull request, no working off of the Develop branch.
* Indenting
* Good commit messages
* No auto-generated build files
* Don't commit code that doesn't compile.

### Construction Process
We will be using Scrum to build our project. Reference [here](http://scrumreferencecard.com/ScrumReferenceCard.pdf).

### Tools Used
* Microsoft Visual Studio 2015
* Twitch API
* Git
* Azure
* Visual Studio Team Services
* Nuget items:
    * Entity
    * Ninject
    * Moq
    

* * *

## Team Members
* Repo Admin/Developer Brandon Hunt Bhunt12@wou.edu
* Developer Kyle Walsh Kwalsh15@wou.edu
* Developer Tyler Connors Tconnors14@wou.edu